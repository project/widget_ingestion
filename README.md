# Widget Ingestion
This module provides pulls the widget definitions from the widget registry and
stores them in [Widget Types](https://www.drupal.org/project/widget_type).
Ingestion operations are deferred to a queue and then processed during cron.
