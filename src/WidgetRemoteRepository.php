<?php

namespace Drupal\widget_ingestion;

use Drupal\Component\Assertion\Inspector;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\widget_type\Entity\WidgetRegistrySource;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\Utils;
use GuzzleHttp\Psr7\Response;
use const DIRECTORY_SEPARATOR;
use const DRUPAL_ROOT;

/**
 * Service to interact with the widget registry.
 */
final class WidgetRemoteRepository {

  public const FORCE_INGESTION_KEY = '_force_ingestion';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * The widget registry endpoints.
   *
   * @var string[]
   */
  private $endpoints = [];

  /**
   * The queue worker.
   *
   * @var \Drupal\Core\Queue\QueueWorkerInterface
   */
  private $queueWorker;

  /**
   * The data from the widget registry.
   *
   * @var array|null
   */
  private $registryData;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * WidgetRemoteRepository constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The guzzle client to make HTTP requests.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    ClientInterface $client,
    QueueWorkerManagerInterface $queue_worker_manager,
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->client = $client;
    $storage = $entity_type_manager->getStorage('widget_registry_source');
    $results = $storage
      ->getQuery()
      ->condition('status', TRUE)
      ->accessCheck()
      ->execute();
    $ids = array_values($results);
    $sources = array_values($storage->loadMultiple($ids));
    $this->endpoints = array_map(function (WidgetRegistrySource $source) {
      return $source->get('endpoint');
    }, $sources);
    $this->queueWorker = $queue_worker_manager
      ->createInstance('widget_ingestion_queue');
    $this->fileSystem = $file_system;
  }

  /**
   * Gets a list of all widgets from the widget registry.
   *
   * @param string[] $endpoints
   *   Endpoints to ingest.
   *
   * @return array
   *   The list of all widgets.
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function listAll(array $endpoints = []): array {
    if (isset($this->registryData)) {
      return $this->registryData;
    }
    $this->registryData = [];
    // Loop over all the endpoints to download all the registries available.
    $promises = array_reduce(
      array_filter($endpoints ?: $this->endpoints),
      function (array $carry, string $endpoint) {
        return array_merge($carry, [$endpoint => $this->client->getAsync(
          $endpoint,
          ['headers' => ['Accept' => 'application/json']]
        )]);
      }, []);
    try {
      // Wait for all promises to resolve.
      $responses = $this->waitAllPromises($promises);
    }
    catch (GuzzleException $exception) {
      watchdog_exception('widget_ingestion', $exception);
      return $this->registryData;
    }
    $bodies = array_map(function (Response $response): array {
      return Json::decode($response->getBody());
    }, $responses);
    $this->registryData = $this->mergeWidgetsFromRegistries($bodies);
    return $this->registryData;
  }

  /**
   * Re-ingest all the widgets.
   *
   * @param bool $force_ingestion
   *   Force ingesting the widget types regardless of the version comparison.
   *
   * @return array[]
   *   The queue items.
   */
  public function ingestAll(bool $force_ingestion = FALSE): array {
    $items = array_map(static function (array $item) use ($force_ingestion) {
      return array_merge($item, [self::FORCE_INGESTION_KEY => $force_ingestion]);
    }, $this->listAll());
    array_map([$this->queueWorker, 'processItem'], $items);
    return $items;
  }

  /**
   * Re-ingest a widget.
   *
   * @param string $short_code
   *   The widget's short code.
   * @param bool $force_ingestion
   *   Force ingesting the widget types regardless of the version comparison.
   *
   * @return array|null
   *   The queued item or NULL if not found.
   */
  public function ingestWidget(string $short_code, string $version, string $endpoint, bool $force_ingestion = FALSE): ?array {
    $all_widgets = $this->listAll();
    $parts = [];
    preg_match(
      '@(v[0-9]+)\.(.*)@',
      $version,
      $parts
    );
    $major_version = $parts[1] ?? 'v1';
    $res = array_filter($all_widgets, static function (array $item) use ($short_code, $major_version, $endpoint) {
      return $item['shortcode'] === $short_code
        && strpos($item['version'], $major_version) !== FALSE &&
        $endpoint === $item['_sourceEndpoint'];
    });
    $widget = reset($res);
    if (!$widget) {
      return NULL;
    }
    $widget[self::FORCE_INGESTION_KEY] = $force_ingestion;
    try {
      $this->queueWorker->processItem($widget);
    }
    catch (\Exception $exception) {
      watchdog_exception('widget_ingestion', $exception);
      return NULL;
    }
    return $widget;
  }

  /**
   * Ingests the assets for the widget type, if necessary.
   *
   * @param string $remote_directory
   *   The remote directory.
   * @param array $files
   *   The list of files to, potentially, ingest.
   * @param string $remote_id
   *   The machine name of the widget definition.
   * @param string $version
   *   The version of the widget.
   *
   * @return \Drupal\widget_ingestion\LocalAssetDescriptor
   *   The asset descriptor to build the Drupal library definition.
   */
  public function ingestAssets(string $remote_directory, array $files, string $remote_id, string $version): LocalAssetDescriptor {
    // All files should be strings.
    assert(Inspector::assertAllStrings($files));
    // We will treat the major versions as different folders.
    $major_version = preg_replace('/(v?\d+)\.\d+\.\d+.*/', '$1', $version);
    $local_dir_wrapper = sprintf('public://widget-data/%s/%s', $remote_id, $major_version);
    $temp_dir = $this->fileSystem->tempnam($this->fileSystem->getTempDirectory(), $remote_id . '-backup-');
    $has_backup = TRUE;
    try {
      $this->fileSystem->move($local_dir_wrapper, $temp_dir);
    }
    catch (FileException $exception) {
      $has_backup = FALSE;
    }
    try {
      array_map(function (string $file) use ($local_dir_wrapper, $remote_directory) {
        $local_file_wrapper = sprintf('%s/%s', $local_dir_wrapper, $file);
        // Ensure that all the intermediary directories are created.
        $dirname = $this->fileSystem->dirname($local_file_wrapper);
        if (!file_exists($dirname)) {
          $this->fileSystem->mkdir($dirname, NULL, TRUE);
        }
        $local_path = $this->fileSystem->realpath($local_file_wrapper);
        $remote_url = sprintf('%s/%s', $remote_directory, $file);
        $this->client->request('GET', $remote_url, ['sink' => $local_path]);
        return $local_path;
      }, $files);
    }
    catch (GuzzleException $exception) {
      // If there is a backup, put it back.
      if ($has_backup) {
        $this->fileSystem->move($temp_dir, $local_dir_wrapper);
      }
    }
    // Now calculate the relative dir path to the widget_type module.
    return new LocalAssetDescriptor(
      $this->relativePath($this->fileSystem->realpath($local_dir_wrapper)),
      $files
    );
  }

  /**
   * Takes a path and a base dir and it makes a relative path.
   *
   * @param string $path
   *   The input path.
   * @param string|null $base_dir
   *   The base directory. If not provided, it defaults to the app root.
   *
   * @return string
   *   The relative path.
   */
  private function relativePath(string $path, string $base_dir = NULL): string {
    if (empty($base_dir)) {
      $base_dir = \Drupal::getContainer()->hasParameter('app.root')
        ? \Drupal::getContainer()->getParameter('app.root')
        : DRUPAL_ROOT;
    }
    $path = preg_replace('/' . preg_quote($base_dir, '/') . '/', '', $path);
    $path = ltrim($path, DIRECTORY_SEPARATOR);
    $levels = count(explode(DIRECTORY_SEPARATOR, drupal_get_path('module', 'widget_type')));
    for ($level = 0; $level < $levels; $level++) {
      $path = '..' . DIRECTORY_SEPARATOR . $path;
    }
    return $path;
  }

  /**
   * Merges the responses from each registry into a single response.
   *
   * @param array $bodies
   *   The bodies from the responses to the multiple registries.
   *
   * @return array[]
   *   The list of widgets to be ingested.
   */
  private function mergeWidgetsFromRegistries(array $bodies): array {
    // For now we just append the responses together along with their source
    // endpoint, if there is more to it when saving the data, then we'll resolve
    // that there.
    $merged = [];
    foreach ($bodies as $endpoint => $bodies_in_endpoint) {
      $data = array_map(function (array $item) use ($endpoint) {
        return array_merge($item, ['_sourceEndpoint' => $endpoint]);
      }, $bodies_in_endpoint);
      $merged = array_merge($merged, $data);
    }
    return $merged;
  }

  /**
   * Waits for a collection of promises to resolve.
   *
   * Guzzle has changed from D8 to D9. We need to do some BC dance.
   *
   * @param \GuzzleHttp\Promise\PromiseInterface[] $promises
   *   The promises.
   *
   * @return mixed
   *   The response.
   *
   * @SuppressWarnings(PHPMD)
   */
  private function waitAllPromises(array $promises) {
    return Utils::all($promises)->wait();
  }

}
