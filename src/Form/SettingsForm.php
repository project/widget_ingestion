<?php

namespace Drupal\widget_ingestion\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Url;

/**
 * Configures widget_ingestion settings for this site.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'widget_ingestion_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['widget_ingestion.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['endpoints'] = $this->buildEndpointsElement($form_state);
    $form['actions']['manual_ingestion'] = [
      '#type' => 'link',
      '#title' => $this->t('Ingest widget definitions now'),
      '#url' => Url::fromRoute('widget_ingestion.manual'),
      '#attributes' => ['class' => ['button', 'button--action']],
      '#weight' => '10',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Builds the endpoints element for the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element.
   */
  private function buildEndpointsElement(FormStateInterface $form_state): array {
    $input_urls = $form_state->getValue(['endpoints', 'urls']);
    $endpoints = empty($input_urls)
      ? $this->config('widget_ingestion.settings')->get('endpoints')
      : $input_urls;
    $element = [
      '#type' => 'fieldset',
      '#title' => $this->t('Endpoint URLs'),
      '#attributes' => [
        'id' => 'endpoints-wrapper',
      ],
      '#tree' => TRUE,
      '#description' => $this->t('Multiple URLs containing the JSON feed with the widget definitions to be parsed. The contents of these JSON endpoints will be imported and new Widget Types may be created or updated.'),
    ];
    $element['urls'] = array_map(function (string $url) {
      return [
        '#type' => 'textfield',
        '#size' => 80,
        '#maxlength' => 255,
        '#default_value' => $url,
      ];
    }, $endpoints);
    $this->appendTextFieldToEndpoints($element);
    $element['add_more'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more items'),
      '#button_type' => 'secondary',
      '#validate' => ['::validateUrl'],
      '#op' => 'add-more-items',
      '#executes_submit_callback' => FALSE,
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'endpoints-wrapper',
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('widget_ingestion.settings');
    $config->set('endpoints', array_unique(array_filter($form_state->getValue([
      'endpoints',
      'urls',
    ]))));
    $config->save();
  }

  /**
   * AJAX callback to update the entire form element.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|array
   *   The form render array or an AJAX response object.
   */
  public function updateFormCallback(array &$form, FormStateInterface $form_state) {
    $complete_form = $form_state->getCompleteForm();
    return $complete_form['endpoints'];
  }

  /**
   * Validates the oEmbed URL.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function validateUrl(array &$form, FormStateInterface $form_state) {
    $urls = $form_state->getValue(['endpoints', 'urls']);
    foreach ($urls as $index => $url) {
      if (!empty($url)) {
        if (!preg_match('/^(ftp|http|https):\/\/[^ "]+$/', $url)) {
          $form_state->setError($form['endpoints']['urls'][$index], $this->t('Invalid URL.'));
        }
        if (array_search($url, array_slice($urls, 0, $index)) !== FALSE) {
          $form_state->setError($form['endpoints']['urls'][$index], $this->t('You can only add each registry URL once.'));
        }
      }
      if (empty($url) && $index !== count($urls) - 1) {
        $form_state->setError($form['endpoints']['urls'][$index], $this->t('URL cannot be empty.'));
      }
    }
  }

  /**
   * Adds a new empty text box at the end of the list of endpoints.
   *
   * @param array $element
   *   The element to modify.
   */
  private function appendTextFieldToEndpoints(array &$element): void {
    $element['urls'][] = [
      '#type' => 'textfield',
      '#size' => 80,
      '#maxlength' => 255,
    ];
  }

}
