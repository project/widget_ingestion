<?php

namespace Drupal\widget_ingestion\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\widget_ingestion\WidgetRemoteRepository;
use Drupal\widget_type\Entity\WidgetRegistrySource;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class to ingest the widgets now.
 */
final class ManualIngestionForm extends ConfirmFormBase {

  /**
   * The repository of widgets.
   *
   * @var \Drupal\widget_ingestion\WidgetRemoteRepository
   */
  private $widgetsRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * WidgetIngestionCommands constructor.
   *
   * @param \Drupal\widget_ingestion\WidgetRemoteRepository $widgets_repository
   *   The repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(WidgetRemoteRepository $widgets_repository, EntityTypeManagerInterface $entity_type_manager) {
    $this->widgetsRepository = $widgets_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(WidgetRemoteRepository::class),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'widget_ingestion_manual';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('widget_registry_source');
    $results = $storage
      ->getQuery()
      ->condition('status', TRUE)
      ->accessCheck()
      ->execute();
    $ids = array_values($results);
    if (empty($ids)) {
      return [
        'empty' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('There are no widget registry sources yet. You need to add at least one source to ingest widgets from it.'),
        ],
        'button' => [
          '#type' => 'link',
          '#title' => $this->t('Add a registry source'),
          '#url' => Url::fromRoute('entity.widget_registry_source.add_form'),
          '#attributes' => [
            'class' => ['button', 'button--primary'],
          ],
        ],
      ];
    }
    $options = array_reduce($storage->loadMultiple($ids),
      function (array $carry, WidgetRegistrySource $source) {
        $endpoint = $source->get('endpoint');
        return array_merge($carry, [
          $endpoint => Link::fromTextAndUrl($source->label(), Url::fromUri($endpoint))
            ->toString()
            ->getGeneratedLink(),
        ]);
      },
      []
    );
    $form = parent::buildForm($form, $form_state);
    $form['endpoints'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Widget Registry Sources'),
      '#description' => $this->t('Endpoints for the widget registry that contain widgets to be ingested, by default all registries will be ingested. You can add more widget registry sources <a href="@href">here</a>.', [
        '@href' => Url::fromRoute('entity.widget_registry_source.add_form')
          ->toString(TRUE)
          ->getGeneratedUrl(),
      ]),
      '#options' => $options,
      '#default_value' => array_keys($options),
    ];
    $form['force_ingestion'] = [
      '#title' => $this->t('Force ingestion'),
      '#description' => $this->t('If checked, all widget definitions will be ingested. If unchecked, only the newer widget definitions will be ingested.'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Ingest the widgets now.
    $force_ingestion = (bool) $form_state->getValue('force_ingestion');
    $items = $this->widgetsRepository->listAll(array_values($form_state->getValue('endpoints')));
    $batch = [
      'title' => $this->t('Ingesting widget definitions'),
      'operations' => array_map(function (array $item) use ($force_ingestion) {
        return [
          __CLASS__ . '::ingestWidget',
          [$item, $force_ingestion],
        ];
      }, $items),
      'finished' => __CLASS__ . '::batchFinished',
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public static function batchFinished($success, $results, $operations, $elapsed_time): void {
    \Drupal::messenger()
      ->addMessage(t('Ingested all widgets in @time: %widgets.', [
        '@time' => $elapsed_time,
        '%widgets' => implode(', ', $results),
      ]));
  }

  /**
   * Processes one batch operation.
   *
   * @param array $data
   *   The widget data.
   * @param bool $force_ingestion
   *   Weather or not to force the ingestion.
   * @param mixed $context
   *   Operates the UI for the batch process.
   */
  public static function ingestWidget(array $data, bool $force_ingestion, &$context) {
    $name = $data['title'];
    $context['message'] = t('Ingesting widget type "@name".', ['@name' => $name]);
    \Drupal::service(WidgetRemoteRepository::class)
      ->ingestWidget($data['shortcode'], $data['version'], $data['_sourceEndpoint'], $force_ingestion);
    $context['results'][] = $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to ingest the widgets from all the registries?');
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function getCancelUrl() {
    return Url::fromRoute('interactive_components');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Ingest');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Manual ingestion will update the widget type entities with the latest information from the widget registry.');
  }

}
