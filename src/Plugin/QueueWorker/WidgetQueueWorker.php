<?php

namespace Drupal\widget_ingestion\Plugin\QueueWorker;

use Composer\Semver\Comparator;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\widget_ingestion\WidgetRemoteRepository;
use Drupal\widget_type\Entity\WidgetType;
use Drupal\widget_type\WidgetRegistrySourceInterface;
use Drupal\widget_type\WidgetTypeConfiguration;
use Drupal\widget_type\WidgetTypeInterface;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use const DIRECTORY_SEPARATOR;

/**
 * Create/update a widget entity.
 *
 * @QueueWorker(
 *   id = "widget_ingestion_queue",
 *   title = @Translation("Widget queue worker"),
 *   cron = {"time" = 3}
 * )
 */
final class WidgetQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * ReportWorkerBase constructor.
   *
   * @param array $configuration
   *   The configuration of the instance.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator service.
   *
   * @SuppressWarnings(PHPMD.LongVariable)
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->updateEntity($data);
  }

  /**
   * Save the widget entity, populated by the correct data.
   *
   * @param array $data
   *   Widget data received from the widget registry.
   *
   * @return \Drupal\widget_type\WidgetTypeInterface
   *   New widget if it does not already exist, or the existing widget.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  private function updateEntity(array $data): WidgetTypeInterface {
    $parts = [];
    preg_match(
      '@(v[0-9]+)\.(.*)@',
      $data['version'],
      $parts
    );
    $major_version_prefix = $parts[1] ?? 'v1';
    $ids = $this->entityTypeManager
      ->getStorage('widget_type')
      ->getQuery()
      ->condition('remote_widget_id', $data['shortcode'])
      ->condition('remote_widget_version', $major_version_prefix . '.%', 'LIKE')
      ->accessCheck(TRUE)
      ->execute();

    $widget_type = $this->deduceWidgetType($ids, $data);
    $is_new_widget_type = !$widget_type;
    if ($is_new_widget_type) {
      $sources = $this->entityTypeManager
        ->getStorage('widget_registry_source')
        ->loadByProperties(['endpoint' => $data['_sourceEndpoint']]);
      $source = reset($sources);
      $widget_type = WidgetType::create([
        'remote_widget_id' => $data['shortcode'],
        'widget_registry_source' => $source,
      ]);
    }
    $force_ingestion = $data[WidgetRemoteRepository::FORCE_INGESTION_KEY] ?? FALSE;
    $skip_ingestion = $ids
      && !$force_ingestion
      && !$is_new_widget_type
      && Comparator::lessThanOrEqualTo(
        $data['version'],
        $widget_type->getVersion()
      );
    if ($skip_ingestion) {
      // Nothing to do.
      return $widget_type;
    }
    $this->importWidget($widget_type, $data);

    // When importing a new widget, invalidate library info cache.
    $widget_exists = (bool) $ids;
    if ($widget_exists && !$widget_type->isNew()) {
      $this->cacheTagsInvalidator->invalidateTags(['library_info']);
    }
    return $widget_type;
  }

  /**
   * Find widget type based on the entity query and the source endpoint.
   *
   * @param array $ids
   *   The ids of the widget type candidates from the entity query.
   * @param array $data
   *   The data object being ingested.
   *
   * @return \Drupal\widget_type\Entity\WidgetType|null
   *   The widget type if it can be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function deduceWidgetType(array $ids, array $data): ?WidgetType {
    $widget_exists = (bool) $ids;
    if (!$widget_exists) {
      return NULL;
    }
    $sources = $this->entityTypeManager
      ->getStorage('widget_registry_source')
      ->loadByProperties(['endpoint' => $data['_sourceEndpoint']]);
    $source = reset($sources);
    if (!$source instanceof WidgetRegistrySourceInterface) {
      return NULL;
    }
    $widget_types = array_filter(
      WidgetType::loadMultiple($ids),
      function (WidgetTypeInterface $wt) use ($source) {
        return $wt->widget_registry_source->target_id === $source->id();
      }
    );
    return reset($widget_types) ?: NULL;
  }

  /**
   * Imports the JS widget.
   *
   * @param \Drupal\widget_type\WidgetTypeInterface $widget_type
   *   The entity being saved.
   * @param array $data
   *   Structured array with widget data.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function importWidget(WidgetTypeInterface $widget_type, array $data): void {
    // Set values from $data, except for the short code which remains invariant.
    $widget_type
      ->setVersion($data['version'])
      ->setDirectory($this->calculateWidgetDirectory($data))
      ->setName($data['title'])
      ->setDescription($data['description'] ?? '')
      ->setSettingsSchema($this->extractSettings($data['settingsSchema'] ?? []))
      ->setFormSchema($data['uiFormSchema'] ?? [])
      ->setFiles($this->extractFiles($data['files'] ?? [], $widget_type->getDirectory()))
      ->setRemoteStatus($data['status'] ?? WidgetTypeInterface::REMOTE_STATUS_UNKNOWN)
      ->setLibraryDependencies($this->extractLibraryDependencies($data));
    $avail_trans = $data['availableTranslations'] ?? NULL;
    if ($avail_trans) {
      $widget_type->setRemoteLanguages($avail_trans);
    }
    $preview_url = $data['preview']['url'] ?? NULL;
    if ($preview_url) {
      $widget_type->setPreviewLink($preview_url);
    }
    $preview_image_url = $data['preview']['thumbnail'] ?? NULL;
    if ($preview_image_url) {
      $preview_image = $this->extractPreviewImage($preview_image_url, $widget_type->getRemoteId());
      if ($preview_image instanceof FileInterface) {
        $widget_type->setPreviewImage($preview_image, $widget_type->getName(), $widget_type->getName());
      }
    }
    $widget_type->save();
  }

  /**
   * Extracts the settings from the widget as a PHP array.
   *
   * @param array $settings
   *   Widget settings schema in JSON Schema format.
   *
   * @return array
   *   An array of widget settings.
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  private function extractSettings(array $settings): array {
    return NestedArray::getValue(
      $settings,
      ['properties', 'fields']
    ) ?: [];
  }

  /**
   * Extracts the list of files in the remote widget definition.
   *
   * @param string[] $files
   *   The list of files compiled by the widget, either JS or CSS.
   * @param string $remote_directory
   *   The remote directory where to find these files.
   *
   * @return string[]
   *   The list of fully qualified URLs with the assets.
   */
  private function extractFiles(array $files, string $remote_directory): array {
    return array_map(static function (string $path) use ($remote_directory): string {
      return UrlHelper::isExternal($path)
        ? $path
        : sprintf('%s/%s', $remote_directory, $path);
    }, $files);
  }

  /**
   * Extracts the list of dependencies in the remote widget definition.
   *
   * If necessary it will download the dependencies to the local file system.
   *
   * @param array $data
   *   The widget data from the registry.
   *
   * @return string[]
   *   The list of fully qualified URLs with the assets.
   */
  private function extractLibraryDependencies(array $data): array {
    $output = [];
    $dependencies = $data['externalPeerDependencies'] ?? [];
    $remote_id = $data['shortcode'];
    $config = new WidgetTypeConfiguration(\Drupal::config('widget_type.settings'));
    $remote_repository = \Drupal::service(WidgetRemoteRepository::class);
    assert($remote_repository instanceof WidgetRemoteRepository);
    foreach ($dependencies as $name => $dependency) {
      try {
        $json_encoded_dep = json_encode($dependency, JSON_THROW_ON_ERROR);
      }
      catch (JsonException $exception) {
        $json_encoded_dep = '';
      }
      $hash = substr(md5($json_encoded_dep), 0, 6);
      $library_id = sprintf('%s-%s', $name, $hash);
      if ($config->shouldIngestAssets($remote_id)) {
        // Make the remote directory / file based on the 'src'.
        $parts = explode('/', $dependency['src']);
        $file = array_pop($parts);
        $remote_directory = implode('/', $parts);
        $assets = $remote_repository->ingestAssets(
          $remote_directory,
          [$file],
          $library_id,
          'external'
        );
        $ingested_file = $assets->getDirectory() . DIRECTORY_SEPARATOR . current($assets->getFiles());
        $dependency['src'] = $ingested_file;
      }
      $output[] = [
        'value' => array_merge(
          $dependency,
          ['id' => $library_id],
        ),
      ];
    }
    return $output;
  }

  /**
   * Calculates the widget directory.
   *
   * If necessary, it also downloads the asset files to the files folder.
   *
   * @param array $data
   *   The widget data from the registry.
   *
   * @return string
   *   The widget directory.
   */
  private function calculateWidgetDirectory(array $data): string {
    $remote_id = $data['shortcode'];
    $config = new WidgetTypeConfiguration(\Drupal::config('widget_type.settings'));
    if (!$config->shouldIngestAssets($remote_id)) {
      return $data['directoryUrl'];
    }
    $remote_repository = \Drupal::service(WidgetRemoteRepository::class);
    assert($remote_repository instanceof WidgetRemoteRepository);
    $assets = $remote_repository->ingestAssets($data['directoryUrl'], $data['files'], $remote_id, $data['version']);
    return $assets->getDirectory();
  }

  private function extractPreviewImage(string $source, string $shortcode): ?FileInterface {
    $parsed = parse_url($source);
    $basename = basename($parsed['path'] ?? '');
    $destination = sprintf('public://widget-types/%s--%s', $shortcode, $basename);
    $file_system = \Drupal::service('file_system');
    $final_destination = $file_system->getDestinationFilename($destination, FileSystemInterface::EXISTS_REPLACE);
    // Try opening the file first, to avoid calling prepareDirectory()
    // unnecessarily. We're suppressing fopen() errors because we want to try
    // to prepare the directory before we give up and fail.
    $destination_stream = @fopen($final_destination, 'w+b');
    if (!$destination_stream) {
      // If fopen didn't work, make sure there's a writable directory in place.
      $dir = $file_system->dirname($final_destination);
      if (!$file_system->prepareDirectory($dir, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        return NULL;
      }
      // Let's try that fopen again.
      $destination_stream = @fopen($final_destination, 'w+b');
      if (!$destination_stream) {
        return NULL;
      }
    }
    // Stream the request body directly to the final destination stream.
    $guzzle_options = ['sink' => $destination_stream];

    try {
      // Make the request. Guzzle throws an exception for anything but 200.
      \Drupal::service('http_client')->get($source, $guzzle_options);
    }
    catch (GuzzleException  $e) {
      return NULL;
    }

    if (is_resource($destination_stream)) {
      fclose($destination_stream);
    }
    $file = File::create(['uri' => $destination]);
    try {
      $file->save();
    }
    catch (EntityStorageException $e) {
      return NULL;
    }
    return $file;
  }
}
