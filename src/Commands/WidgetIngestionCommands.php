<?php

namespace Drupal\widget_ingestion\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\widget_ingestion\WidgetRemoteRepository;
use Drush\Commands\DrushCommands;

/**
 * Drush command file for widget ingestion.
 */
class WidgetIngestionCommands extends DrushCommands {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The repository of widgets.
   *
   * @var \Drupal\widget_ingestion\WidgetRemoteRepository
   */
  private $widgetsRepository;

  /**
   * WidgetIngestionCommands constructor.
   *
   * @param \Drupal\widget_ingestion\WidgetRemoteRepository $widgets_repository
   *   The repository.
   */
  public function __construct(WidgetRemoteRepository $widgets_repository) {
    parent::__construct();
    $this->widgetsRepository = $widgets_repository;
  }

  /**
   * Processes one batch operation.
   *
   * @param array $data
   *   The widget data.
   * @param bool $force_ingestion
   *   Weather or not to force the ingestion.
   * @param mixed $context
   *   Operates the UI for the batch process.
   */
  public function ingestWidget(array $data, bool $force_ingestion, &$context) {
    $name = $data['title'];
    $context['message'] = t('Ingesting widget type "@name".', ['@name' => $name]);
    $this->widgetsRepository->ingestWidget($data['shortcode'], $data['version'], $data['_sourceEndpoint'], $force_ingestion);
    $context['results'][] = $data['title'];
  }

  /**
   * Ingests all widgets from the associated widget registry.
   *
   * @param array $options
   *   The command options.
   *
   * @field-labels
   *   id: ID
   *   name: Name
   *   version: Version
   *   web_segments: Web Segments
   *   translations: Remote Translations
   * @default-fields id,name,version
   *
   * @option force-ingestion
   *   If supplied, the widget will be ingested regardless of the version
   *   comparison between Drupal and the widget registry.
   *
   * @command widget_ingestion:ingest
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   The ingested widget types.
   */
  public function ingest(array $options = ['force-ingestion' => FALSE]) {
    // Ingest the widgets now.
    $force_ingestion = $options['force-ingestion'];
    $items = $this->widgetsRepository->listAll();
    $batch = [
      'title' => $this->t('Ingesting widget definitions'),
      'operations' => array_map(function (array $item) use ($force_ingestion) {
        return [
          [$this, 'ingestWidget'],
          [$item, $force_ingestion],
        ];
      }, $items),
    ];
    batch_set($batch);
    drush_backend_batch_process();
    $rows = array_map(static function (array $item) {
      return [
        'id' => $item['shortcode'],
        'name' => $item['title'],
        'version' => $item['version'],
        'translations' => implode(', ', $item['availableTranslations'] ?? []),
      ];
    }, $items);
    return new RowsOfFields($rows);
  }

}
