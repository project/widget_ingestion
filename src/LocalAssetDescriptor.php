<?php

namespace Drupal\widget_ingestion;

/**
 * Value object that holds information about where assets are ingested.
 */
class LocalAssetDescriptor {

  /**
   * The assets directory.
   *
   * @var string
   */
  private $directory;

  /**
   * The files.
   *
   * @var string[]
   */
  private $files;

  /**
   * Constructs a LocalAssetDescriptor.
   *
   * @param string $directory
   *   The asset directory.
   * @param string[] $files
   *   The files.
   */
  public function __construct(string $directory, array $files) {
    $this->directory = $directory;
    $this->files = $files;
  }

  /**
   * Gets the directory.
   *
   * @return string
   *   The directory.
   */
  public function getDirectory(): string {
    return $this->directory;
  }

  /**
   * Gets the files.
   *
   * Examples: '../../widget-data/example-widget/v1/js/main.js'.
   *
   * @return string[]
   *   The files.
   */
  public function getFiles(): array {
    return $this->files;
  }

}
