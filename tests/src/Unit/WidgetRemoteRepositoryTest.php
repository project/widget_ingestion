<?php

namespace Drupal\Tests\widget_ingestion\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\QueueWorkerInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\widget_ingestion\WidgetRemoteRepository;
use Drupal\widget_type\Entity\WidgetRegistrySource;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Unit tests for the WidgetRemoteRepository.
 *
 * @group widget_ingestion
 *
 * @coversDefaultClass \Drupal\widget_ingestion\WidgetRemoteRepository
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
final class WidgetRemoteRepositoryTest extends UnitTestCase {

  /**
   * The system under test.
   *
   * @var \Drupal\widget_ingestion\WidgetRemoteRepository
   */
  private $theSut;

  /**
   * The queue worker.
   *
   * @var \Drupal\Core\Queue\QueueWorkerInterface
   */
  private $queueWorker;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $storage = $this->prophesize(EntityStorageInterface::class);
    $wrs1 = $this->prophesize(WidgetRegistrySource::class);
    $wrs1->get('endpoint')->willReturn('foo-bar');
    $wrs2 = $this->prophesize(WidgetRegistrySource::class);
    $wrs2->get('endpoint')->willReturn('foo-bar-2');
    $storage->loadMultiple(Argument::type('array'))->willReturn([
      'wrs_1' => $wrs1->reveal(),
      'wrs2' => $wrs2->reveal(),
    ]);
    $query = $this->prophesize(QueryInterface::class);
    $query_w_condition = $this->prophesize(QueryInterface::class);
    $query_w_access_check = $this->prophesize(QueryInterface::class);
    $query_w_access_check->execute()->willReturn(['wrs_1' => 'wrs_1']);
    $query_w_condition->accessCheck()->willReturn($query_w_access_check->reveal());
    $query->condition('status', TRUE)->willReturn($query_w_condition->reveal());
    $storage->getQuery()->willReturn($query->reveal());
    $entity_type_manager->getStorage('widget_registry_source')->willReturn($storage->reveal());
    $options = ['headers' => ['Accept' => 'application/json']];
    $response = new Response(200, [], '[{"shortcode":"python","version":"v5.4.3"},{"lorem": "ipsum","shortcode":"foo","version":"v1.2.3"}]');
    $this->queueWorker = $this->prophesize(QueueWorkerInterface::class);
    $this->queueWorker->processItem(Argument::type('array'))->willReturn();
    $queue_worker_manager = $this->prophesize(QueueWorkerManagerInterface::class);
    $queue_worker_manager->createInstance(Argument::type('string'))
      ->shouldBeCalledTimes(1)
      ->willReturn($this->queueWorker->reveal());
    $client = $this->prophesize(Client::class);
    $promise = new Promise();
    $promise->resolve($response);
    $client->getAsync('foo-bar', $options)->willReturn($promise);
    $promise2 = new Promise();
    $promise2->resolve(new Response(200, [], '[]'));
    $client->getAsync('foo-bar-2', $options)->willReturn($promise2);
    $file_system = $this->prophesize(FileSystemInterface::class);
    $this->theSut = new WidgetRemoteRepository(
      $client->reveal(),
      $queue_worker_manager->reveal(),
      $file_system->reveal(),
      $entity_type_manager->reveal()
    );
  }

  /**
   * @covers ::listAll
   */
  public function testListAll() {
    self::assertEquals([
      ['shortcode' => 'python', 'version' => 'v5.4.3', '_sourceEndpoint' => 'foo-bar'],
      ['lorem' => 'ipsum', 'shortcode' => 'foo', 'version' => 'v1.2.3', '_sourceEndpoint' => 'foo-bar'],
    ], $this->theSut->listAll());
  }

  /**
   * @covers ::ingestAll
   */
  public function testIngestAll() {
    $this->queueWorker
      ->processItem(Argument::type('array'))
      ->shouldBeCalledTimes(2);
    self::assertEquals(
      [
        ['shortcode' => 'python', 'version' => 'v5.4.3', '_sourceEndpoint' => 'foo-bar', '_force_ingestion' => FALSE],
        ['lorem' => 'ipsum', 'shortcode' => 'foo', 'version' => 'v1.2.3', '_sourceEndpoint' => 'foo-bar', '_force_ingestion' => FALSE],
      ],
      $this->theSut->ingestAll()
    );
  }

  /**
   * @covers ::ingestWidget
   */
  public function testIngestWidget() {
    $this->queueWorker
      ->processItem(Argument::type('array'))
      ->shouldBeCalledTimes(1);
    self::assertEquals(
      ['lorem' => 'ipsum', 'shortcode' => 'foo', 'version' => 'v1.2.3', '_sourceEndpoint' => 'foo-bar', '_force_ingestion' => TRUE],
      $this->theSut->ingestWidget('foo', 'v1.99.99', 'foo-bar', TRUE)
    );
  }

}
