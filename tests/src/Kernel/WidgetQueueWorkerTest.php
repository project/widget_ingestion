<?php

namespace Drupal\Tests\widget_ingestion\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\widget_type\Entity\WidgetRegistrySource;
use Drupal\widget_type\Entity\WidgetType;
use Drupal\widget_type\WidgetTypeInterface;

/**
 * Kernel tests for WidgetQueueWorker.
 *
 * @group widget_ingestion
 *
 * @coversDefaultClass \Drupal\widget_ingestion\Plugin\QueueWorker\WidgetQueueWorker
 */
final class WidgetQueueWorkerTest extends KernelTestBase {

  /**
   * The system under test.
   *
   * @var \Drupal\widget_ingestion\Plugin\QueueWorker\WidgetQueueWorker
   */
  private $theSut;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'widget_ingestion',
    'widget_type',
    'field',
    'file',
    'image',
    'text',
    'user',
    'system',
  ];

  /**
   * {@inheritDoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::setContainer($this->container);
    $this->installEntitySchema('widget_type');
    $this->installEntitySchema('user');
    $this->installConfig(['field', 'widget_type', 'user', 'image']);
    $this->theSut = $this->container
      ->get('plugin.manager.queue_worker')
      ->createInstance('widget_ingestion_queue');
    $source = WidgetRegistrySource::create([
      'id' => 'source-foo',
      'endpoint' => 'http://foo-the/bar',
    ])->save();
    WidgetType::create([
      'name' => 'The name',
      'remote_widget_id' => 'remote-id',
      'remote_widget_version' => 'v1.2.3',
      'remote_widget_directory' => 'https://the-s3/path',
      'remote_widget_status' => 'stable',
      'widget_registry_source' => 'source-foo',
    ])->save();
  }

  /**
   * @covers ::processItem
   * @dataProvider providerTestProcessItem
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function testProcessItem($data, $is_new, $is_updated) {
    $old_widget_types = WidgetType::loadMultiple();
    $this->theSut->processItem($data);
    $widget_types = WidgetType::loadMultiple();
    $widget_type = current(array_filter(
      $widget_types,
      static function (WidgetTypeInterface $widget_type) use ($data) {
        return $widget_type->getRemoteId() === $data['shortcode'];
      }
    ));
    if (!$is_new) {
      // If the widget was not updated, that means that the input version was
      // not persisted.
      $method = $is_updated ? 'assertEquals' : 'assertNotEquals';
      self::{$method}($data['version'], $widget_type->getVersion());
    }
    self::assertCount(
      $is_new ? count($old_widget_types) + 1 : count($old_widget_types),
      $widget_types
    );
    if ($is_new || $is_updated) {
      $expected = [
        'name' => [['value' => $data['title']]],
        'status' => [['value' => '1']],
        'description' => [['value' => $data['description'], 'format' => NULL]],
        'remote_widget_id' => [['value' => $data['shortcode']]],
        'remote_widget_version' => [['value' => $data['version']]],
        'remote_widget_status' => [['value' => $data['status']]],
        'remote_widget_directory' => [['value' => $data['directoryUrl']]],
        'available_translation_languages' => array_map(
          static function (string $tr) {
            return ['value' => $tr];
          },
          $data['availableTranslations']
        ),
      ];
      $actual = $widget_type->toArray();
      self::assertEquals(
        array_intersect_key($expected, $actual),
        array_intersect_key($actual, $expected)
      );
      if (isset($data['settingsSchema'])) {
        self::assertEquals(
          $data['settingsSchema']['properties']['fields'],
          $widget_type->getSettingsSchema()
        );
      }
    }
  }

  /**
   * Data provider for testProcessItem.
   *
   * @return array
   *   The data.
   */
  public function providerTestProcessItem() {
    return [
      [
        [
          'shortcode' => 'lorem-ipsum',
          'version' => 'v1.0.0',
          'directoryUrl' => 'foo',
          'title' => 'bar',
          'description' => 'baz',
          'status' => 'deprecated',
          'availableTranslations' => ['ca-es'],
          '_sourceEndpoint' => 'http://foo-the/bar',
        ],
        TRUE,
        FALSE,
      ],
      [
        [
          'shortcode' => 'remote-id',
          'version' => 'v1.0.0',
          'directoryUrl' => 'foo',
          'title' => 'bar',
          'description' => 'baz',
          'status' => 'wip',
          '_sourceEndpoint' => 'http://foo-the/bar',
        ],
        FALSE,
        FALSE,
      ],
      [
        [
          'shortcode' => 'remote-id',
          'version' => 'v1.2.4',
          'directoryUrl' => 'foo',
          'title' => 'bar',
          'description' => 'baz',
          'status' => 'stable',
          'settingsSchema' => ['properties' => ['fields' => ['f1' => 'one']]],
          'availableTranslations' => ['ca-es', 'eu-es'],
          '_sourceEndpoint' => 'http://foo-the/bar',
        ],
        FALSE,
        TRUE,
      ],
    ];
  }

}
